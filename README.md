About
===========
This plugin uses Sentinel-2 L2A images to calculate an NDVI and REIP vegetationindex for a specific polygon.


Usage
===========
First you need a polygon as .shp file additionally you also need to download Band4, Band5, Band6, Band7, Band8 Sentinel-2 L2A sateliteimages from https://apps.sentinel-hub.com/eo-browser/
These bands are important for calculate NDVI and REIP index of your polygon.
The sateliteimages are downloaded as a .zip Archiv. If you start the plugin you need to choose your polygon as "Feldgrenzen" and for "Archiv" choose your downloaded images.