# -*- coding: utf-8 -*-
"""
/***************************************************************************
 FertilizerMap
                                 A QGIS plugin
 Creates fertilizer map
                             -------------------
        begin                : 2018-07-24
        copyright            : (C) 2018 by GIS-ELA
        email                : lukas.hauer@josephinum.at
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load FertilizerMap class from file FertilizerMap.

    :param iface: A QGIS interface instance.
    :type iface: QgisInterface
    """
    #
    from .fertilizer_map import FertilizerMap
    return FertilizerMap(iface)
